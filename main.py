#!/usr/bin/env python

import pygame
from pygame.locals import *
from spriteFactory import *


# Not a lot happens in this module, it handles setting everything up and processing user inputs, most of the rest of the
# brains of the program live in spriteFactory module.


NUMBER_OF_ALIENS = 4 # controls how many aliens we'll have on screen.


def main():
    ''' Main program, run the game :) '''

    # General Pygame init stuff:
    #------------------------------------
    pygame.init()
    screen = pygame.display.set_mode((1280, 720)) #ToDo: consider making this dynamic. Fixed screen size is a little weird...


    # Setup sprites
    #------------------------------------
    ship = shipSprite()
    alienList = alienSwarm(NUMBER_OF_ALIENS)
    allsprites = pygame.sprite.RenderPlain((ship, alienList.getAliens()))
    allsprites.draw(screen)

    # Main loop:
    #------------------------------------
    while (True):
        #ToDo add keydown event to catch escape key to toggle a menu
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        quit()
        keysDown = pygame.key.get_pressed()
        if keysDown[pygame.K_LEFT]:
            ship.doAction("left")
        if keysDown[pygame.K_RIGHT]:
            ship.doAction("right")

        #ToDo: add spacebar check here to extend to add firing

        #ToDo: currently animating based on clockrate, change this to a standard refresh rate.
        alienList.updateAlienPosition()
        screen.fill((0,0,0))
        allsprites.draw(screen)

        pygame.display.flip()

if __name__ == "__main__":
    main()