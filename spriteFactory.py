
#!/usr/bin/env python
import pygame
from pygame.locals import *
import os, sys

# Most of the meat of this program is in this module, which handles generating and moving around sprites.

# Note -  all sprites open source. See readme for credits and sources.

def load_image(name, colorkey=None):
    """ this code came directly from https://www.pygame.org/docs/tut/ChimpLineByLine.html """
    fullname = os.path.join('resources/images/', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image:', name
        raise SystemExit, message
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()

class alienSprite(pygame.sprite.Sprite):
    """ This class is a single alien sprite. Note: See alienSwarm class to see what the computer does to move them around."""
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) 
        self.image, self.rect = load_image('alien.png', -1)


class shipSprite(pygame.sprite.Sprite):
    """ This class handles the player sprite and actions."""

    def __init__(self,speed=10):
        """ Creates the player and puts them at the bottom center of the screen. Speed is optional argument to control player movement."""
        pygame.sprite.Sprite.__init__(self) 
        self.image, self.rect = load_image('ship.png', -1)
        left, top, width, height = self.rect
        self.rect = (left+600, top+650, width, height)
        self.speed = speed

    def doAction(self,direction):
        """ Handles player actions such as movement. This function takes a string ("right" | "left") ."""
        left, top, width, height = self.rect
        if direction == "left":
            self.rect = left-self.speed, top, width, height
                
        elif direction=="right":
            self.rect =left+self.speed, top, width, height

class alienSwarm():
    """ This class makes a swarm of aliens, and handles moving them around on screen."""

    def __init__(self, numberOfAliens, speed=10):
        """ Creates a swarm of aliens. numberOfAliens controls how many there are, speed is optional argument to control movement speed"""
        self.aliens = self.generateAliens(numberOfAliens)
        self.length = numberOfAliens
        self.direction = 1
        self.speed = speed

    def getAliens(self):
            return self.aliens

    def generateAliens(self,numberOfAliens):
        """ Makes a list of alien sprite objects, spaced out 100px apart """
        alienArray = []
        for i in range (0,numberOfAliens):
            alien = alienSprite()
            alien.image = pygame.transform.scale(alien.image, (50,50))
            left, top, width, height = alien.rect
            alien.rect = ((100*i),top,50, 50)
            alienArray.append(alien)
        return alienArray
        
   
    def updateAlienPosition(self):
        """ Moves aliens along the screen, bouncing off the edges. Speed is controlled by member variable "speed" """
        #check if last alien is touching right edge of screen
        left, top, width, height = self.aliens[self.length-1].rect
        if (left+width) >=1280:
            self.direction = -1

        #check if first alien is touching left edge of screen
        left, top, width, height = self.aliens[0].rect
        if left <=0:
            self.direction= 1

        #update position of each alien in correct direction
        for i in range (0, self.length):
                left, top, width, height = self.aliens[i].rect
                self.aliens[i].rect = (((left+(self.speed*self.direction))), top, width,height)